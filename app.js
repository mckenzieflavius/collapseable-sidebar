let open = true;

function toggleCollapse() {
    const nav = document.querySelector('.sidebar');

    if (open) {
    console.log('it is open, closing now');
        nav.classList.add('closed');
        nav.classList.remove('open');
        open = !open;
    } else {
        console.log('it is closed, opening now');
        nav.classList.add('open');
        nav.classList.remove('closed');
        open = !open;
    }
}